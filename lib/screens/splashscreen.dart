import 'package:flutter/material.dart';
import 'package:homedix/screens/homepage.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const HomePage(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: const Color(0xFF018C97),
      backgroundColor: const Color.fromARGB(255, 198, 238, 238),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logowithoutdesc.png',
              height: 300.0,
              width: 300.0,
            ),
            const SizedBox(height: 20.0),
            const CircularProgressIndicator(color: Colors.green),
          ],
        ),
      ),
    );
  }
}
final List<String> imageUrls = [
  'https://picsum.photos/250?image=9',
  'https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.systematic.com%2Fmedia%2Fg0sj1tbg%2Fhospital-building-001-global.jpg%3FcropAlias%3Dhero_large%26width%3D992%26height%3D483%26quality%3D80%26rmode%3Dcrop%26format%3Dwebp%26null&tbnid=Kr8cmyfx1hHGxM&vet=12ahUKEwjYr4DSgOv_AhUPTKQEHQY0DWcQMygIegUIARDzAQ..i&imgrefurl=https%3A%2F%2Fsystematic.com%2Fen-gb%2Findustries%2Fhealthcare%2Fhospital-care%2F&docid=c9LDuWl_3F8ESM&w=992&h=483&q=hospital%20images&client=ubuntu&ved=2ahUKEwjYr4DSgOv_AhUPTKQEHQY0DWcQMygIegUIARDzAQ',
  'https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.britannica.com%2F27%2F93827-050-A91D558F%2FPatient-dialysis-treatment.jpg&tbnid=U8HlI_UIfQc_7M&vet=12ahUKEwjYr4DSgOv_AhUPTKQEHQY0DWcQMygUegUIARCLAg..i&imgrefurl=https%3A%2F%2Fwww.britannica.com%2Fscience%2Fhospital%2FThe-general-hospital&docid=TsWRtlAYtrw41M&w=1600&h=1108&q=hospital%20images&client=ubuntu&ved=2ahUKEwjYr4DSgOv_AhUPTKQEHQY0DWcQMygUegUIARCLAg',
];

class ImageSlideshow extends StatefulWidget {
  @override
  _ImageSlideshowState createState() => _ImageSlideshowState();
}

class _ImageSlideshowState extends State<ImageSlideshow> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 2),
      ),
      items: imageUrls.map((imageUrl) {
        return Container(
          child: Image.network(
            imageUrl,
            fit: BoxFit.cover,
          ),
        );
      }).toList(),
    );
  }
}

